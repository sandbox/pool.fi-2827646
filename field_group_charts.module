<?php
/**
 * @file
 * Field group charts module.
 */

/**
 * Implements hook_field_group_formatter_info().
 */
function field_group_charts_field_group_formatter_info() {
  module_load_include('inc', 'charts', 'includes/charts.pages'); 
  return array(
    'display' => array(
      'chart' => array(
        'label' => t('Chart'),
        'description' => t('This fieldgroup renders its children as a chart.'),
        'instance_settings' => charts_default_settings(),
      ),
    ),
  );
}

/**
 * Implements hook_field_group_format_settings().
 */
function field_group_charts_field_group_format_settings($group) {
  $form = array();
  switch ($group->format_type) {
    case 'chart':
      module_load_include('inc', 'charts', 'includes/charts.pages'); 
      $parents = array('fields', $group->group_name, 'format_settings', 'settings', 'instance_settings');
      $form['instance_settings']['#tree'] = TRUE;
      $form['instance_settings'] += charts_settings_form($form, $group->format_settings['instance_settings'], array(), $parents);
  }
  return $form;
}

/**
 * Implements	hook_field_group_pre_render_alter().
 */
function field_group_charts_field_group_pre_render_alter(&$element, $group, &$form) {
  if ($group->format_type == 'chart') {
    $settings = $group->format_settings['instance_settings'];
    $chart = field_group_charts_get_element_from_settings($settings);
    $labels = array();
    $data = array();
    foreach (element_children($element) as $field_name) {
      if (isset($element[$field_name]['#items'])) {
        $value = 0;
        foreach ($element[$field_name]['#items'] as $item) {
          if (isset($item['value']) && is_numeric($item['value'])) {
            $value += $item['value'];
          }
        }
        $labels[] = $element[$field_name]['#title'];
        $data[] = $value;
      }
    }
    $chart['data'] = array(
      '#type' => 'chart_data',
      '#title' => t('Gender'),
      '#labels' => $labels,
      '#data' => $data,
    );
    $element = $chart;
  }
  return $element;
}

/**
 * Create a chart element from settings.
 *
 * Mostly stolen from charts/views/charts_plugin_style_chart.inc
 */
function field_group_charts_get_element_from_settings($settings) {
  $chart = array(
    '#type' => 'chart',
    '#chart_type' => $settings['type'],
    '#chart_library' => $settings['library'],
    '#title' => $settings['title_position'] ? $settings['title'] : FALSE,
    '#title_position' => $settings['title_position'],
    '#colors' => $settings['colors'],
    '#background' => $settings['background'] ? $settings['background'] : 'transparent',
    '#legend' => $settings['legend_position'] ? TRUE : FALSE,
    '#legend_position' => $settings['legend_position'] ? $settings['legend_position'] : NULL,
    '#width' => $settings['width'],
    '#height' => $settings['height'],
  );
  return $chart;
  /*
  $chart_type_info = chart_get_type($settings['type']);

  if ($chart_type_info['axis'] === CHARTS_SINGLE_AXIS) {
    $data_field_key = key($data_fields);
    $data_field = $data_fields[$data_field_key];

    $data = array();
    $renders = $this->render_fields($this->view->result);
    foreach ($renders as $row_number => $row) {
      $data_row = array();
      if ($label_field_key) {
        // Labels need to be decoded, as the charting library will re-encode.
        $data_row[] = htmlspecialchars_decode($renders[$row_number][$label_field_key], ENT_QUOTES);
      }
      // Convert empty strings to NULL.
      if ($renders[$row_number][$data_field_key] === '') {
        $value = NULL;
      }
      else {
        $value = (float) $renders[$row_number][$data_field_key];
      }
      $data_row[] = $value;
      $data[] = $data_row;
    }

    if ($label_field) {
      $chart['#legend_title'] = $label_field->options['label'];
    }

    $chart[$this->view->current_display . '_series'] = array(
      '#type' => 'chart_data',
      '#data' => $data,
      '#title' => $data_field->options['label'],
    );
  }
  else {
    $chart['xaxis'] = array(
      '#type' => 'chart_xaxis',
      '#title' => $this->options['xaxis_title'] ? $this->options['xaxis_title'] : FALSE,
      '#continuous' => $this->options['xaxis_continuous'],
      '#labels_rotation' => $this->options['xaxis_labels_rotation'],
    );
    $chart['yaxis'] = array(
      '#type' => 'chart_yaxis',
      '#title' => $this->options['yaxis_title'] ? $this->options['yaxis_title'] : FALSE,
      '#labels_rotation' => $this->options['yaxis_labels_rotation'],
      '#max' => $this->options['yaxis_max'],
      '#min' => $this->options['yaxis_min'],
    );
    foreach ($data_fields as $field_key => $field_handler) {
      $chart[$this->view->current_display . '__' . $field_key] = array(
        '#type' => 'chart_data',
        '#data' => array(),
        '#color' => isset($this->options['field_colors'][$field_key]) ? $this->options['field_colors'][$field_key] : NULL,
        '#title' => $field_handler->options['label'],
        '#prefix' => $this->options['yaxis_prefix'] ? $this->options['yaxis_prefix'] : NULL,
        '#suffix' => $this->options['yaxis_suffix'] ? $this->options['yaxis_suffix'] : NULL,
        '#decimal_count' => $this->options['yaxis_decimal_count'] ? $this->options['yaxis_decimal_count'] : NULL,
      );
    }

    $renders = $this->render_fields($this->view->result);
    foreach ($renders as $row_number => $row) {
      if ($label_field_key) {
        $chart['xaxis']['#labels'][] = $renders[$row_number][$label_field_key];
      }
      foreach ($data_fields as $field_key => $field_handler) {
        // Convert empty strings to NULL.
        if ($renders[$row_number][$field_key] === '') {
          $value = NULL;
        }
        // Strip thousands placeholders if present, then cast to float.
        else {
          $value = (float) str_replace(array(',', ' '), '', $renders[$row_number][$field_key]);
        }
        $chart[$this->view->current_display . '__' . $field_key]['#data'][] = $value;
      }
    }
  }

  // Check if this display has any children charts that should be applied
  // on top of it.
  $parent_display_id = $this->view->current_display;
  $children_displays = $this->get_children_chart_displays();
  foreach ($children_displays as $child_display_id => $child_display) {
    // If the user doesn't have access to the child display, skip.
    if (!$this->view->access($child_display_id)) {
      continue;
    }

    // Generate the subchart by executing the child display. We load a fresh
    // view here to avoid collisions in shifting the current display while in
    // a display.
    $subview = $this->view->clone_view();
    $subview->set_display($child_display_id);

    // Copy the settings for our axes over to the child view.
    foreach ($this->options as $option_name => $option_value) {
      if (strpos($option_name, 'yaxis') === 0 && $child_display->handler->get_option('inherit_yaxis')) {
        $subview->display_handler->options['style_options'][$option_name] = $option_value;
      }
      elseif (strpos($option_name, 'xaxis') === 0) {
        $subview->display_handler->options['style_options'][$option_name] = $option_value;
      }
    }

    // Execute the subview and get the result.
    $subview->pre_execute();
    $subview->execute();

    // If there's no results, don't attach the subview.
    if (empty($subview->result)) {
      continue;
    }

    $subchart = $subview->style_plugin->render($subview->result);
    $subview->post_execute();
    unset($subview);

    // Create a secondary axis if needed.
    if (!$child_display->handler->get_option('inherit_yaxis') && isset($subchart['yaxis'])) {
      $chart['secondary_yaxis'] = $subchart['yaxis'];
      $chart['secondary_yaxis']['#opposite'] = TRUE;
    }

    // Merge in the child chart data.
    foreach (element_children($subchart) as $key) {
      if ($subchart[$key]['#type'] === 'chart_data') {
        $chart[$key] = $subchart[$key];
        // If the subchart is a different type than the parent chart, set
        // the #chart_type property on the individual chart data elements.
        if ($subchart['#chart_type'] !== $chart['#chart_type']) {
          $chart[$key]['#chart_type'] = $subchart['#chart_type'];
        }
        if (!$child_display->handler->get_option('inherit_yaxis')) {
          $chart[$key]['#target_axis'] = 'secondary_yaxis';
        }
      }
    }
  }
  */
  return $chart;
}